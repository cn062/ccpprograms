#include<stdio.h>
void vowel(char *c,int *f);
void main()
{
    char ch;
    int flag=0;
    printf("Enter character\n");
    scanf("%c",&ch);
    vowel(&ch,&flag);
    if(flag==1)
    {
        printf("Yes,the character is  vowel");
    }
    else
    {
        printf("No,the character is not  vowel");
    }
}
void vowel(char *c,int *f)
{
    if(*c=='a'||*c=='e'||*c=='i'||*c=='o'||*c=='u'||*c=='A'||*c=='E'||*c=='I'||*c=='O'||*c=='U')
    {
        *f=1;
    }
}
#include<stdio.h>
void swap(int *a,int *b);
void main()
{
    int num1,num2;
    printf("Enter two numbers\n");
    scanf("%d%d",&num1,&num2);
     printf("The numbers before swapping were 1st=%d and 2nd=%d\n",num1,num2);
    swap(&num1,&num2);
    printf("The numbers after swapping are 1st=%d and 2nd=%d",num1,num2);
}
void swap(int *a,int *b)
{
    int temp;
    temp=*b;
    *b=*a;
    *a=temp;
}
