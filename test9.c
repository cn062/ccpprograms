#include<stdio.h>
void arithmetic(int *a,int *b,int *sum,int *sub,int *mult,float *divi,int *rem);
void main()
{
    int num1,num2,sum,sub,mult,rem;
    float divi;
    printf("Enter two numbers\n");
    scanf("%d%d",&num1,&num2);
    arithmetic(&num1,&num2,&sum,&sub,&mult,&divi,&rem);
    printf("The sum is %d\n",sum);
    printf("The difference is %d\n",sub);
    printf("The multiplication is %d\n",mult);
    printf("The divison is %f\n",divi);
    printf("The remainder is %d\n",rem);
}
void arithmetic(int *a,int *b,int *sum,int *sub,int *mult,float *divi,int *rem)
{
    *sum =(*a)+(*b);
    if((*a)>(*b))
    {
    *sub = (*a)-(*b);
    *divi = (float)(*a)/(float)(*b);
    *rem = (*a) % (*b);
    }
    else
    {
    *sub = (*b)-(*a);
    *divi = (float)(*b) /(float)(*a);
    *rem = ((*b) % (*a));
    }
    *mult = (*a) * (*b);
}//write your code here
