#include<stdio.h>
#include<stdlib.h>
int main()
{
 FILE *file1, *file2;
 char ch;
 file1=fopen("file1","r");
 if(file1==NULL)
 {
  puts("cannot open the file");
  exit(0);
 }
 file2=fopen("file2","w");
 if(file2==NULL)
 {
  puts("cannot open the target file");
  fclose(file1) ;
  exit(0);
 }
 while(1)
 {
  ch=fgetc(file1);
  if(ch==EOF)
   break;
  else
   fputc(ch,file2);
 }
 fclose(file1);
 fclose(file2);
 return 0 ;
}